//
//  ViewController.swift
//  oekaki
//
//  Created by 吉村研人 on 2019/02/17.
//  Copyright © 2019 吉村研人. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var ActivityIndicator: UIActivityIndicatorView!
    var animator: UIViewPropertyAnimator!
    var animatorReverse: UIViewPropertyAnimator!
    var anim: Bool!
    
    struct JsonSample: Codable {
        let id: String
        let predictions: [Image]
        let processedTime: String
        let status: String
        
        struct Image: Codable {
            let imageName: String
            let results: [Result]
            
            struct Result: Codable {
                let boundingBox: Bounding
                let text: String
                let bboxAccuracy: Double
                
                struct Bounding: Codable {
                    let x1: Double
                    let x2: Double
                    let x3: Double
                    let x4: Double
                    let y1: Double
                    let y2: Double
                    let y3: Double
                    let y4: Double
                }
            }
        }
    }
    
    @IBAction func reset(_ sender: UIButton) {
        self.canvas.image = initDrawImage
        lastDrawImage = initDrawImage
        DispatchQueue.main.async {
            self.inputText.text = ""
            self.wordCount.text = ""
            self.image.image = UIImage(named: "White")!
//            self.imageKemuri.image = UIImage(named: "Toumei")
            self.imageKemuri.image = nil
            
            if (self.anim == true){
                self.animatorReverse.startAnimation()
                self.anim = false
            }

        }
    }

    @IBOutlet weak var imageKemuri: UIImageView!
    @IBOutlet weak var inputText: UILabel!
    @IBAction func ocr(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.ActivityIndicator.startAnimating()
            self.resetButton.isEnabled = false
            self.ocrButton.isEnabled = false
        }

        
        print("押されたよ")
        let boundaryConstant = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
        let headers = [
            "APIKey": "jcxG0EwpTuOFYsAoG5lB7emkU251GuKe"
        ]
        let contentType = "multipart/form-data; boundary=" + boundaryConstant
        //API endpoint for API sandbox
        var request = URLRequest(url: URL(string: "https://sandbox.api.sap.com/ml/scenetextrecognition/scene-text-recognition")!)
        //setting request method
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        let session = URLSession.shared
        
        
//        let path1 = Bundle.main.path(forResource: "your_image", ofType: "png")!
//        let url = URL(fileURLWithPath: path1)
//        let fileName = url.lastPathComponent
        let fileName = "sample.png"
        //        let data = try? Data(contentsOf: url)
//        let imageData = UIImage.init(data: data!)!
        //let imageData = self.canvas.image!
        
        
        let tmpImageData = GetImage()
        let imageData = resize(image: tmpImageData, width: 100)
        
//        let imageData = UIImage(named: "Image")!
        let pngData = imageData.pngData()!
        
        print(pngData.base64EncodedString())
        
        let mimeType = "image/png"
        
        let boundaryStart = "--\(boundaryConstant)\r\n"
        let boundaryEnd = "--\(boundaryConstant)--\r\n"
        let fieldName = "files"
        let contentDispositionString = "Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(fileName)\"\r\n"
        let contentTypeString = "Content-Type: \(mimeType)\r\n\r\n"
        
        var body = Data()
        
        body.append(boundaryStart.data(using: .utf8)!)
        body.append(contentDispositionString.data(using: .utf8)!)
        body.append(contentTypeString.data(using: .utf8)!)
        body.append(pngData)
        body.append("\r\n".data(using: .utf8)!)
        body.append(boundaryEnd.data(using: .utf8)!)
        request.httpBody = body
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        request.setValue(String(body.count), forHTTPHeaderField: "Content-Length")
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if (error != nil) {
                print(error)
            } else {

                DispatchQueue.main.async {
                    self.ActivityIndicator.stopAnimating()
                    self.resetButton.isEnabled = true
                    self.ocrButton.isEnabled = true
                }
                
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)

                let json = try? JSONDecoder().decode(JsonSample.self, from: data!)

                if (json?.predictions[0].results.count == 0){
                    print("読み込めなかったからもう一回")
                    DispatchQueue.main.async {
                        self.inputText.text = "読み込めなかったからもう一回"
                        self.wordCount.text = "0"
                    }
                } else {
                    print(json?.predictions[0].results.count.description)
                    
                    var output = ""
                    
                    for x in (json?.predictions[0].results)!{
                        output = output + " " + x.text
                    }
                    
                    DispatchQueue.main.async {
                        self.inputText.text = output
                        self.wordCount.text = json?.predictions[0].results.count.description
                        
                        if (output == " CA" || output == " ca" || output == " Ca"){
                            self.image.image = UIImage(named: "Ca")!
                        }
                        
                        if (output == " Dog"){
                            self.image.image = UIImage(named: "Dog")
                        }
                        if (output == " Cat" || output == " cat"){
                            self.image.image = UIImage(named: "Cat")
                        }

                        if (output == " Jump"){
                            self.animator.startAnimation()
                            self.anim = true
                            self.imageKemuri.image = UIImage(named: "Kemuri")
                        }
                        
                    }
                    print(output)
                }

            }
        }
        dataTask.resume()
        

    }
    @IBAction func cleanTextOnly(_ sender: UIButton) {
        self.canvas.image = initDrawImage
        lastDrawImage = initDrawImage
        DispatchQueue.main.async {
            self.inputText.text = ""
            self.wordCount.text = ""
//            self.image.image = UIImage(named: "White")!
//            self.imageKemuri.image = UIImage(named: "White")
        }
        
    }
    
    @IBOutlet weak var wordCount: UILabel!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var ocrButton: UIButton!
    @IBOutlet weak var image: UIImageView!
    
    var bezierPath:UIBezierPath!
    
    var canvas:UIImageView!
    
    var lastDrawImage:UIImage?
    
    var initDrawImage:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        anim = false
        
        image.image = UIImage(named: "White")!
//        imageKemuri.image = UIImage(named: "Toumei")
        
        animator = UIViewPropertyAnimator(duration:1.0,curve: .easeInOut){
            self.image.center.y -= 100
        }
        animatorReverse = UIViewPropertyAnimator(duration:1.0,curve: .easeInOut){
            self.image.center.y += 100
        }
        
        
        // ActivityIndicatorを作成＆中央に配置
        ActivityIndicator = UIActivityIndicatorView()
        ActivityIndicator.frame = CGRect(x: 120, y: 60, width: 50, height: 50)
        ActivityIndicator.center = self.view.center
        
        // クルクルをストップした時に非表示する
        ActivityIndicator.hidesWhenStopped = true
        
        // 色を設定
        ActivityIndicator.style = UIActivityIndicatorView.Style.gray
        
        //Viewに追加
        self.view.addSubview(ActivityIndicator)
        
        let width = self.view.frame.width
        let height = self.view.frame.height
        //キャンバスを設置
        canvas = UIImageView()
        canvas.frame = CGRect(x:0,y:400,width:width,height:300)
        canvas.backgroundColor = UIColor.white
        self.initDrawImage = canvas.image
        self.view.addSubview(canvas)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchEvent = touches.first!
        let currentPoint:CGPoint = touchEvent.location(in: self.canvas)
        bezierPath = UIBezierPath()
        bezierPath.lineWidth = 4.0
        bezierPath.lineCapStyle = .butt
        bezierPath.move(to:currentPoint)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if bezierPath == nil {
            return
        }
        let touchEvent = touches.first!
        let currentPoint:CGPoint = touchEvent.location(in: self.canvas)
        bezierPath.addLine(to: currentPoint)
        drawLine(path: bezierPath)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if bezierPath == nil {
            return
        }
        let touchEvent = touches.first!
        let currentPoint:CGPoint = touchEvent.location(in: canvas)
        bezierPath.addLine(to: currentPoint)
        drawLine(path: bezierPath)
        self.lastDrawImage = canvas.image
    }
    
    //描画処理
    func drawLine(path:UIBezierPath){
        UIGraphicsBeginImageContext(canvas.frame.size)
        if let image = self.lastDrawImage {
            image.draw(at: CGPoint.zero)
        }
        let lineColor = UIColor.black
        lineColor.setStroke()
        path.stroke()
        self.canvas.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
    func GetImage() -> UIImage{
        
        // キャプチャする範囲を取得.
        let rect = self.canvas.bounds
        
        // ビットマップ画像のcontextを作成.
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        let context: CGContext = UIGraphicsGetCurrentContext()!
        
        // 対象のview内の描画をcontextに複写する.
        self.canvas.layer.render(in: context)
        
        // 現在のcontextのビットマップをUIImageとして取得.
        let capturedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        // contextを閉じる.
        UIGraphicsEndImageContext()
        
        return capturedImage
    }

    func resize(image: UIImage, width: Double) -> UIImage {
        
        // オリジナル画像のサイズからアスペクト比を計算
        let aspectScale = image.size.height / image.size.width
        
        // widthからアスペクト比を元にリサイズ後のサイズを取得
        let resizedSize = CGSize(width: width, height: width * Double(aspectScale))
        
        // リサイズ後のUIImageを生成して返却
        UIGraphicsBeginImageContext(resizedSize)
        image.draw(in: CGRect(x: 0, y: 0, width: resizedSize.width, height: resizedSize.height))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resizedImage!
    }
}

// gitテスト
// feature/test/01の修正
